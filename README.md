# infra-images

**Proof of concept**. This has become
https://gitlab.com/gitlab-com/gl-infra/ci-images.

See [issue](TODO) and line comments in [.gitlab-ci.yml](.gitlab-ci.yml) for
context.
